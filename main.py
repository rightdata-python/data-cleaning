from typing import List, Optional
from fastapi import FastAPI
from pydantic import BaseModel
from fastapi import FastAPI
from api import Identify_Few_Values,remove_col_Few_Values
from api import Identify_col_Low_Variance,remove_col_Low_Variance
from api import Identify_SingleValue,removing_SingleValue
from api import Identify_duplicate_rows,removing_duplicate_rows
from api import dedupe_example, delete_row

#assigning FastAPI as app
app = FastAPI()

class datacleaning(BaseModel):
    #driver dataset location
    inputFile: str
    outputFile:str

@app.post("/Identify_SingleValue")
#calling the functions with given inputs
def Identify(bm: datacleaning):
    Result = Identify_SingleValue(bm.inputFile,bm.outputFile)
    return {f"{Result}"}

class datacleaning(BaseModel):
    #driver dataset location
    inputFile: str
    outputFile:str

@app.post("/removing_SingleValue")
#calling the functions with given inputs
def removing(dm: datacleaning):
    Result = removing_SingleValue(dm.inputFile,dm.outputFile)
    return {f"{Result}"}


class Col(BaseModel):
    inputFile: str
    outputFile:str

@app.post("/identifyfewvalues")
#calling the functions with given inputs
def Identify_Few(fm:Col):
    Result = Identify_Few_Values(fm.inputFile,fm.outputFile)
    return {f"{Result}"}

class RemoveCol(BaseModel):
    inputFile: str
    outputFile:str

@app.post("/remove_col_Few_Values")
#calling the functions with given inputs
def remove_col_Few(rm:RemoveCol):
    Result = remove_col_Few_Values(rm.inputFile,rm.outputFile)
    return {f"{Result}"}

class Coll(BaseModel):
    inputFile: str
    outputFile:str

@app.post("/Identify_col_Low_Variance")
#calling the functions with given inputs
def Identify_col_Low(cm:Coll):
    Result = Identify_col_Low_Variance(cm.inputFile,cm.outputFile)
    return {f"{Result}"}

class RemoveColl(BaseModel):
    inputFile: str
    outputFile:str

@app.post("/remove_col_low_Values")
#calling the functions with given inputs
def remove_col_Low(km:RemoveColl):
    Result = remove_col_Low_Variance(km.inputFile,km.outputFile)
    return {f"{Result}"}

class datacleaning_rows(BaseModel):
    #driver dataset location
    inputFile: str
    outputFile:str

@app.post("/Identify_duplicate_rows")
#calling the functions with given inputs
def Identify_duplicate(jm: datacleaning_rows):
    Result = Identify_duplicate_rows(jm.inputFile,jm.outputFile)
    return {f"{Result}"}


class removingrows(BaseModel):
    #driver dataset location
    inputFile: str
    outputFile:str

@app.post("/removing_duplicate_value")
#calling the functions with given inputs
def removing_duplicate(lm: removingrows):
    Result = removing_duplicate_rows(lm.inputFile,lm.outputFile)
    return {f"{Result}"}


class Dedupeexample(BaseModel):
    inputFile: str
    duplicatefile:str
    uniquefile:str

@app.post("/dedupeexample")
#calling the functions with given inputs
def dedupe_code(fm:Dedupeexample):
    Result = dedupe_example(fm.inputFile,fm.duplicatefile,fm.uniquefile)
    return {f"{Result}"}



class Deleterow(BaseModel):
    inputfile: str
    columnname:list[str]
    outputfile:str

@app.post("/delete_row")
#calling the functions with given inputs
def dedupe_code(fm:Deleterow):
    Result = delete_row(fm.inputfile,fm.columnname,fm.outputfile)
    return {f"{Result}"}
