from typing import List, Optional
from fastapi import FastAPI
from pydantic import BaseModel
#importing Models as definitions from the files
from iqr_example import iqr_for_all_columns
from iqr_example import percentile_capping,role_based

#assigning FastAPI as app
app = FastAPI()

class IqrTech(BaseModel):
    inputfile : str
    columnvalues: list

@app.post("/iqr")
#calling the functions with given inputs
async def Iqr(fm:IqrTech):
    Result = iqr_for_all_columns(fm.inputfile,fm.columnvalues)

    return {f"{Result}"}



class PercentileCapping(BaseModel):
    columnname : str
    filename : str
    maxthreshold : float
    minthreshold : float
    outputfile : str

@app.post("/percentilecapping")
#calling the functions with given inputs
async def Percentile(pc:PercentileCapping):
    Result = percentile_capping(pc.columnname,pc.filename,pc.maxthreshold,pc.minthreshold,pc.outputfile)

    return {f"{Result}"}


class RoleBasedCapping(BaseModel):
    inputfile : str
    lowerbound : int
    upperbound : int
    columnnames : str
    outputfile : str

@app.post("/rolebasedcapping")
#calling the functions with given inputs
async def RoleBased(rb:RoleBasedCapping):
    Result = role_based(rb.inputfile,rb.lowerbound,rb.upperbound,rb.columnnames,rb.outputfile)
    return {f"{Result}"}






