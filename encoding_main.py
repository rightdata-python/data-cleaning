from typing import List, Optional
from fastapi import FastAPI
from pydantic import BaseModel
#importing Models as definitions from the files
from encoding_techniques import OneHotEncoding,Ordinal_Encoding

#assigning FastAPI as app
app = FastAPI()

class Onehotencodingtechnique(BaseModel):
    inputfile : str
    columnames: list
    outputfile: str

@app.post("/OneHotEncoding")
#calling the functions with given inputs
async def OneHotEncoding(fm:Onehotencodingtechnique):
    Result = OneHotEncoding(fm.inputfile,fm.columnames,fm.outputfile)

    return {f"{Result}"}


class Ordinal_Encodingtechnique(BaseModel):
    inputfile : str
    columnames: list
    outputfile: str

@app.post("/Ordinal_Encoding")
#calling the functions with given inputs
async def Ordinal_Encoding(fm:Ordinal_Encodingtechnique):
    Result = Ordinal_Encoding(fm.inputfile,fm.columnames,fm.outputfile)

    return {f"{Result}"}