import numpy as np
import pandas as pd
# import category_encoders as ce

def OneHotEncoding(inputfile,columnames,outputfile):

    data = pd.read_csv(inputfile)
    print(data.head())
    data[columnames].value_counts()
    # data['attitude'].value_counts()
    one_hot_encoded_data = pd.get_dummies(data, columns = columnames)
    print(one_hot_encoded_data)
    one_hot_encoded_data.to_csv(outputfile)

# OneHotEncoding("C:/Users/gvsph/PycharmProjects/OutlierCapping/Encoding/data.csv",["favcolour","fav_day"],"C:/Users/gvsph/PycharmProjects/OutlierCapping/Encoding/hot.csv")

def Ordinal_Encoding(inputfile,columnnames,outputfile):

    df = pd.read_csv(inputfile)

    # create an object of the OrdinalEncoding
    ce_ordinal = pd.OrdinalEncoder(cols=columnnames)
    # fit and transform and you will get the encoded data
    result = ce_ordinal.fit_transform(df)
    result = result.to_csv(outputfile)
    return result

# Ordinal_Encoding("C:/Users/gvsph/PycharmProjects/OutlierCapping/pokemon.csv",['Speed','Name'],"OrdinaryEncoding.csv")