import pandas as pd

def iqr_for_all_columns(inputfile,columnvalues=[]):
    # col_vals = ["HP"]
    df = pd.read_csv(inputfile)
    df1 = df.copy()
    df = df._get_numeric_data()

    q1 = df.quantile(0.25)
    q3 = df.quantile(0.75)

    iqr = q3 - q1

    lower_bound = q1 - (1.5 * iqr)
    upper_bound = q3 + (1.5 * iqr)

    print(lower_bound)
    print(upper_bound)
    # df_no_outlier = iqr[(iqr.col_vals>lower_bound)&(iqr.col_vals<upper_bound)]

    for col in columnvalues:
        for i in range(0, len(df[col])):
            if df[col][i] < lower_bound[col]:
                df[col][i] = lower_bound[col]

            if df[col][i] > upper_bound[col]:
                df[col][i] = upper_bound[col]

    for col in columnvalues:
        df1[col] = df[col]
    print("-------------------",df1)
    return (df1)


def percentile_capping(columnname,filename,maxthreshold,minthreshold,outputfile):

    df = pd.read_csv(filename)
    df.head()
    #qunatile will returns the value at the first quartile(q1) of the dataset data .
    max_thresold = df[columnname].quantile(maxthreshold)
    print(max_thresold)
    hii = df[df[columnname]>max_thresold]
    min_thresold = df[columnname].quantile(minthreshold)
    print(min_thresold)
    print(df[df[columnname]<min_thresold])
    rmv_outliers = df[(df[columnname]<max_thresold) & (df[columnname]>min_thresold)]
    print("-----------------",rmv_outliers)
    rmv_outliers.to_csv(outputfile)
    return rmv_outliers


def role_based(inputfile, lowerbound, upperbound, columnnames,outputfile):
    # making data frame
    data = pd.read_csv(inputfile)

    # passing values to new column
    data[columnnames] = data[columnnames].clip(lower=lowerbound, upper=upperbound)

    print(data)
    data.to_csv(outputfile)
    return data

# iqr_for_all_columns("C:/Users/gvsph/PycharmProjects/OutlierCapping/pokemon.csv",["HP"])

