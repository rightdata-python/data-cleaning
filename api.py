from numpy import arange
from pandas import read_csv
from sklearn.feature_selection import VarianceThreshold
from matplotlib import pyplot
import pandas as pd
# path = 'C:/Users/gvsph/PycharmProjects/DataCleaning/oil-spill.csv'

#------------------------Identify Columns That Contain a Single Value--------------------

# loads the dataset directly from the URL and prints the number of unique values for each column.
# (or) the column index, and the number of unique values for each column.
def Identify_SingleValue(inputFile,outputFile):
    df = read_csv(inputFile)
    counts = df.nunique()
    print("***********************")
    print(counts)
    counts.to_csv(outputFile)

# identified columns are then removed from the DataFrame,
# and the number of rows and columns in the DataFrame are reported to confirm the change.
def removing_SingleValue(inputFile,outputFile):
    df = read_csv(inputFile , header= 'infer')
    nunique = df.nunique()
    print(nunique)
    cols_to_drop = nunique[nunique == 1].index
    print(cols_to_drop)
    df.drop(cols_to_drop, axis=1 , inplace=True)
    print(df)
    df.to_csv(outputFile)

#-------------Consider Columns That Have Very Few Values------------------
#we refer to this columns to remove low values between 1 and 0
#Consider Columns That Have Very Few Values
def Identify_Few_Values(inputFile,outputFile):
    df = read_csv(inputFile, header=None)
    print(df.shape)
    counts = df.nunique()
    to_del = [i for i,v in enumerate(counts) if (float(v)/df.shape[0]*100) < 1]
    print(to_del)
    # df.to_csv('C:/Users/gvsph/PycharmProjects/DataCleaning/out_Identify_Few_Values_emp.csv')
    df.to_csv(outputFile)
    print(df.shape)
    return "success"

#Consider Columns That Have Very Few Values and droping them.
def remove_col_Few_Values(inputFile,outputFile):
    df = read_csv(inputFile, header=None)
    print(df.shape)
    counts = df.nunique()
    to_del = [i for i,v in enumerate(counts) if (float(v)/df.shape[0]*100) < 1]
    print(to_del)
    df.drop(to_del, axis=1, inplace=True)
    # df.to_csv('C:/Users/gvsph/PycharmProjects/DataCleaning/out_remove_col_Few_Values.csv')
    df.to_csv(outputFile)
    print(df.shape)
    return "success"

#----------Remove Columns That Have A Low Variance------------


#Columns That Have A Low Variance
#Recall that the variance is a statistic calculated on a variable as the average squared difference
# of values on the sample from the mean.
def Identify_col_Low_Variance(inputFile,outputFile):
    df = read_csv(inputFile, header=None)
    # split data into inputs and outputs
    data = df.values
    # print("----data--------",data)
    X = data[:, :-1]
    # print("----X--------",X)
    y = data[:, -1]
    print(X.shape, y.shape)
    # define thresholds to check
    transform = VarianceThreshold()
    # apply transform with each threshold
    X_sel = transform.fit_transform(X)
    print("-----")
    print(X_sel.shape)
    # df.to_csv('C:/Users/gvsph/PycharmProjects/DataCleaning/out_Identify_col_Low_Variance.csv')
    df.to_csv(outputFile)
    print(df.shape)
    return "success"

# Removing Columns That Have A Low Variance
# comparing variance threshold to the number of selected features
def remove_col_Low_Variance(inputFile,outputFile):
    # load the dataset
    df = read_csv(inputFile, header=None)
    # split data into inputs and outputs
    data = df.values
    X = data[:, :-1]
    y = data[:, -1]
    print(X.shape, y.shape)
    # define thresholds to check
    thresholds = arange(0.0, 0.55, 0.05)
    # apply transform with each threshold
    results = list()
    for t in thresholds:
        # define the transform
        transform = VarianceThreshold(threshold=t)
        # transform the input data
        X_sel = transform.fit_transform(X)
        # determine the number of input features
        n_features = X_sel.shape[1]
        print('>Threshold=%.2f, Features=%d' % (t, n_features))
        # store the result
        results.append(n_features)
    # plot the threshold vs the number of selected features
    pyplot.plot(thresholds, results)
    pyplot.show()
    # df.to_csv('C:/Users/gvsph/PycharmProjects/DataCleaning/out_remove_col_Low_Variance.csv')
    df.to_csv(outputFile)
    print(df.shape)
    return "success"

#---------Identify Rows That Contain Duplicate Data------


# Identify Rows That Contain Duplicate Data
def Identify_duplicate_rows(inputFile,outputFile):
    df = read_csv(inputFile, header=None)
    # calculate duplicates
    dups = df.duplicated()
    # report if there are any duplicates
    print(dups.any())
    # list all duplicate rows
    print(df[dups])
    # df.to_csv('C:/Users/gvsph/PycharmProjects/DataCleaning/out_Identify_duplicate_rows.csv')
    df.to_csv(outputFile)
    print(df.shape)
    return "success"

# Removie Identify Rows That Contain Duplicate Data
def removing_duplicate_rows(inputFile,outputFile):
    df = read_csv(inputFile, header=None)
    # calculate duplicates
    dups = df.duplicated()
    # report if there are any duplicates
    print(dups.any())
    # list all duplicate rows
    print(df[dups])
    df.drop_duplicates(inplace=True)
    print(df.shape)
    # df.to_csv('C:/Users/gvsph/PycharmProjects/DataCleaning/out_removing_duplicate_rows.csv')
    df.to_csv(outputFile)
    print(df.shape)
    return "success"

#Dedupe
def dedupe_example(inputfile,duplicatefile,uniquefile):

    x = pd.read_csv(inputfile)

    duplicates = x.duplicated(['Site name','Address'], keep = False)
    #use the names of the columns you want to check

    x[duplicates].to_csv(duplicatefile) #write duplicates

    x[~duplicates].to_csv(uniquefile) #write uniques

# dedupe_example("C:/Users/gvsph/PycharmProjects/dedupe-examples-master/dedupe-examples-master/csv_example/csv_example_messy_input.csv","C:/Users/gvsph/PycharmProjects/dedupe-examples-master/dedupe-examples-master/csv_example/duplicates.csv","C:/Users/gvsph/PycharmProjects/dedupe-examples-master/dedupe-examples-master/csv_example/uniques.csv")



def delete_row(inputfile,columnname,outputfile):
    # read_csv function which is used to read the required CSV file
    data = pd.read_csv(inputfile)

    # display
    print("Original 'input.csv' CSV Data: \n")
    print(data)

    # drop function which is used in removing or deleting rows or columns from the CSV files
    data.drop(columnname, inplace=True, axis=1)

    # display
    print("\nCSV Data after deleting the column 'year':\n")
    print(data)
    data.to_csv(outputfile)
    # data.to_csv('C:/Users/gvsph/PycharmProjects/DataCleaning/bhavi.csv')
